@extends('adminLTE.master')

@section('judul')
    Edit Data Cast
@endsection

@section('content')
    <form role="form" action="/cast/{{ $cast->id }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" name="nama" class="form-control" value="{{ old('nama', $cast->nama) }}" id="nama">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" name="umur" class="form-control" value="{{ old('umur', $cast->umur) }}" id="umur">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" rows="3" name="bio" id="bio">{{ old('bio', $cast->bio) }}</textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary w-100">Submit</button>
    </form>
@endsection
