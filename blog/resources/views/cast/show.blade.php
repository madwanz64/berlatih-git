@extends('adminLTE.master')

@section('judul')
    Data Cast
@endsection

@section('sessionAlert')
@endsection

@section('content')
    <div class="px-3">
        <h3>{{ $cast->nama }}</h3>
        <p>Umur : {{ $cast->umur }} tahun</p>
        <p>{{ $cast->bio }}</p>
        <div class="row d-flex justify-content-start">
            <a href="/cast" class="btn btn-info mx-2">Kembali</a>
            <a href="/cast/{{ $cast->id }}/edit" class="btn btn-outline-info mx-2">Edit</a>
            <form action="/cast/{{ $cast->id }}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" onclick="return confirm('Delete cast?')" class="btn btn-danger mx-2">
            </form>

        </div>
    </div>
@endsection
