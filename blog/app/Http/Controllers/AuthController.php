<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('auth.register');
    }
    public function welcome(Request $registerFill){
        $firstname = $registerFill['firstname'];
        $lastname = $registerFill['lastname'];
        return view('auth.welcome', compact('firstname', 'lastname'));
    }
}
