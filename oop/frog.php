<?php
require_once("animal.php");
class Frog extends Animal
{

    function __construct($input)
    {
        $this->name = $input;
        $this->printAnimal();
    }
    function jump()
    {
        echo "Jump : Hop Hop<br>";
    }
}
