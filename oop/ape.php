<?php
require_once("animal.php");
class Ape extends Animal
{
    public $legs = 2;
    function __construct($input)
    {
        $this->name = $input;
        $this->printAnimal();
    }
    function yell()
    {
        echo "Yell : Auooo<br>";
    }
}
