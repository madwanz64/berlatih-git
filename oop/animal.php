<?php

class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";

    function __construct($input)
    {
        $this->name = $input;
    }
    function get_name()
    {
        echo "Name : $this->name<br>";
    }
    function get_legs()
    {
        echo "Legs : $this->legs<br>";
    }
    function get_cold_blooded()
    {
        echo "Cold Blooded : $this->cold_blooded<br>";
    }
    function printAnimal()
    {
        $this->get_name();
        $this->get_legs();
        $this->get_cold_blooded();
    }
}
